// Authors : __MinoPB__//__Jcob21__   
// environment : SCILAB 6.02
// Date of creation : 2020-01-03 

                // Authors : __MinoPB__//_Jcob21 _  
                // environment : SCILAB 
                // Date of creation :2019.12.19
                // Function servs for meaking histogram

function PeakHistogram(Values, DigitPoints, File_Name)
    scf(0)                  // set the current graphic figure, its needed to create pdf file 
    x=[0:1:255]             // x values on histogram 
    plot2d3(x, Values ,style=2,rect=[110, 0, 125, DigitPoints * 1.1])  // size of Input Matrix , Input Matrix, color(psitive intiger), rect = (min x, min y, max x max y)
    xtitle("values of collected data ")
    xs2pdf(0,File_Name)     // command  that transfers histogram to pdf file, the fiist value (in this case 0) is current graph and the second as you can see is filename  
endfunction


                // Authors : __MinoPB__//__Jcob21__   
                // environment : SCILAB 6.02
                // Date of creation :2020.01.07
                // function serves for creating a data buffor

function [Main_Matrix] = Data_Buffer(Fd, Elements_Amount,Starting_Pos)

    
    mseek(Starting_Pos, Fd,'set')
    Main_Matrix = mget(Elements_Amount, 'uc')
    
endfunction

                // Author : __MinoPB__   
                // environment : SCILAB 6.02
                // Date of creation: 2019-12-25
                // function serves for observ noise level in input signal
function [Digit_Points, Noise_Lvl, PulseLvl] = NoiseLevel(File_Name)
    while  ~meof(Fd)  do
        
        Position = mtell(Fd)       //shows actualy position in file
        Elements_Amount = 1.5e9
        Main_Matrix = Data_Buffer(Fd, Elements_Amount, Position)        // Creating data buffer
        
        for n = 1:length(Main_Matrix)
            PulseLvl(Main_Matrix(n)) = PulseLvl(Main_Matrix(n)) + 1      //counting how many times this value appear in matrix. (value == matrix cell's index)
        end
    end
[DigitPoints, NoiseLvl] = max(PulseLvl)                     // DigitPoints - Maximum value in Pulselvl matrix, NoiseLvl - level of background signal value 
endfunction


                // Author : __MinoPB__/__Jcob21__
                // environment : SCILAB 6.02
                // Date of creation: 2019-12-25
                // function serves for counting Peaks in output signal of A/C Converter (?!))
function [PeakCount, PulseLvl, Track, Summary, Position, DigitPoints] = Count_Pulses(File_Name, dt, Start_Position) //dt >> time resolution, pos>> start position in file
   
    Fd = mopen(File_Name ,'rb')             // Open file 
    PulseLvl = zeros(1:256)                 // Matrix with data about numbers of appereance any values in file 
    PeakCount = 0                           //number of peaks in input signal
    m = 0                                   // marker -shows if signal is growing
    Summary = 0                             // value of field under the analized part of signal - integral 
    Elements_Amount = 1.5e9
    
    while  ~meof(Fd)  do
        
        Position = mtell(Fd)                                            //shows actualy position in file
        Main_Matrix = Data_Buffer(Fd, Elements_Amount, Position)        // Creating data buffer
        
        for n = 3:length(Main_Matrix)
            AnalizeMatrix = Main_Matrix(n-2:n);                          // pl:wczytywanie kolejnych wartości do macierzy analizy 
                                                                            en: loadning further values to AnalizeMatrix
            
            
            
            dy = AnalizeMatrix(2) - AnalizeMatrix(1)                    //pl: obliczenie zmiany wartości w kolejnych elementach sygnału
                                                                        // en : counting the change of the value in further elemnts of signal 
                if m == 0  // if looking for grow
                        if dy > 0
                            X0 = AnalizeMatrix(1)
                            m = 1
                            Summary = Summary + dy*dt
                        end
                else       // if grow was spoted earlier
                    dY = AnalizeMatrix(2)- X0
                    
                    if AnalizeMatrix(1) >= X0
    
                        if AnalizeMatrix(2) ~= X0
                            Summary = Summary + dY*dt
                            
                        elseif AnalizeMatrix(3) < X0 
                            Summary = Summary + dY*dt
                            
                        else
                                if Summary  > 3 * 10^-9         //there is a peak...
                                    PeakCount = PeakCount + 1
                                    Summary = 0
                                    m = 0
                                else                            // or is not.
                                    Summary = 0
                                    m = 0
                                end
                        end
                        
                    else   //if AnalizeMatrix(1) < X0
                        if AnalizeMatrix(2) ~= X0
                            Summary = Summary + dY*dt
                            
                        elseif AnalizeMatrix(3) < X0 //and AnalizeMatrix(4) < AnalizeMatrix(3)
                            Summary = Summary + dY*dt
                            if Summary  > 3 * 10^-9         //there is a peak...
                                    PeakCount = PeakCount + 1
                                    Summary = 0
                                    m = 0
                            else                            // or is not.
                                    Summary = 0
                                    m = 0
                            end
                        else
                                if Summary  > 3 * 10^-9         //there is a peak...
                                    PeakCount = PeakCount + 1
                                    Summary = 0
                                    m = 0
                                else                            // or is not.
                                    Summary = 0
                                    m = 0
                                end
                            end 
                         end
                end
        end
    end

mclose('all')

endfunction


// example comends for running this functions
//File_Name = 'BigData.dat'
//dt = 5e-10                                           // time of one impulse [s]
//Start_Pos = 0                                        // starting position in file

//[PeakCount, Summary, pos] = Count_Pulses(File_Name, dt, Start_Pos);

//[Digit_Points, Noise_Lvl, PulseLvl] = NoiseLevel(File_Name)

//PeakHistogram(PulseLvl, Digit_Points, File_Name)

save(BigDataPaekAnalisis.dat, PeakCount, Noise_Lvl, PulseLvl) 

